import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Router, Routes,RouteReuseStrategy } from '@angular/router';

import { ProductModule } from './products/product.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './home/welcome.component';



const  routes: Routes = [
  { path: 'welcome', component: WelcomeComponent },
  { path: '', redirectTo: 'welcome', pathMatch: 'full'},
  { path: '**', redirectTo: 'welcome', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    // application components for this module
    AppComponent,
    WelcomeComponent
  ],
  // Modules for 3rd party components/directives/pipes
  imports: [
    BrowserModule, // only imported in root app module,use CommonModule in other modules
    HttpClientModule,
    RouterModule.forRoot(routes),
    // app modules
    ProductModule
  ],
  // starting point of app defined
  bootstrap: [AppComponent],
  providers: []
})
export class AppModule { }
