
export interface IProduct {
  productId: number;
  productName: string;
  productCode: string;
  releaseDate: string;
  price: number;
  description: string;
  starRating: number;
  imageUrl: string;

}

//  EXAMPLE of CLASS USING INTERFACE

// export class Product implements IProduct {

//   public productId: number;
//   public productName: string;
//   public productCode: string;
//   public releaseDate: string;
//   public price: number;
//   public description: string;
//   public starRating: number;
//   public imageUrl: string;

//   constructor( ) { }

//   calculateDiscount(percent: number ): number {
//     return this.price - (this.price * percent / 100);
//   }
// }
