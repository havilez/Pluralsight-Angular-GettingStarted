import { Component, OnInit } from '@angular/core';

import { IProduct } from './product';
import { ProductService } from './product.service';

@Component({
  // selector not needed because we are routing to this component
  // selector: 'pm-products',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})

export class ProductListComponent implements OnInit {
  pageTitle: string = 'Product List';
  imageWidth: number = 50;
  imageMargin: number = 2;
  showImage: boolean = false;
  errorMessage: string;

  // private _productService: ProductService;

  _listFilter: string;
  // getter/setter keep track of changed value used input field
  get listFilter(): string {
    return this._listFilter;
  }
  set listFilter( value: string ) {
    this._listFilter = value;

    this.filteredProducts = this.listFilter ? this.performFilter(this.listFilter ) : this.products;
  }

  filteredProducts: IProduct[];
  products: IProduct[] = [];

  // products: IProduct[] =  [
  //     {
  //         'productId': 2,
  //         'productName' : 'Garden Cart',
  //         'productCode' : 'GDN-0023',
  //         'releaseDate' : 'March 18, 2016',
  //         'description': '15 gallon capacity rolling garden cart',
  //         'price': 32.99,
  //         'starRating' : 4.2,
  //         'imageUrl' : 'http://openclipart.org/image/300px/svg_to_png/58471/garden_cart.png'
  //     },
  //     {
  //       'productId': 5,
  //       'productName': 'Hammer',
  //       'productCode': 'TBX-0048',
  //       'releaseDate': 'May 21, 2016',
  //       'description': 'Curved claw steel hammer',
  //       'price': 8.9,
  //       'starRating': 4.8,
  //       'imageUrl': 'http://openclipart.org/image/300px/svg_to_png/73/rejon_Hammer.png'
  //     }
  // ];

  // equivalent way to inject service into a component
  // constructor(productService: ProductService) {
  //   this._productService = productService;

  constructor(private _productService: ProductService) {


  }

  ngOnInit(): void {
    console.log('In OnInit');
    this._productService.getProducts()
    // subscribe(valueFn, errorFn)
  .subscribe( (products) => {
                this.products = products
                this.filteredProducts = this.products;
              },
              (error) => {
                this.errorMessage = <any>error
              });



    this.listFilter = '';

  }

  // event handler for StarComponent @Output() ratingClicked
  onRatingClicked(message: string): void {
    this.pageTitle = 'Product List: ' + message;
   }

  toggleImage(): void {
      this.showImage = !this.showImage;
  }

  performFilter(filterBy: string): IProduct[] {
    // convert filter criteria to lower case
    filterBy = filterBy.toLocaleLowerCase();

    return this.products.filter( (product: IProduct) => {
        // if filter criteria is found in element then add element to product list
        product.productName.toLocaleLowerCase().indexOf(filterBy) !== -1
    });
  }
}
