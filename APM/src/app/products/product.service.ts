import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { IProduct } from './product';


@Injectable()
export class ProductService {
  //FIX-ME: retrive from local file, eventually use web server lib, like webpack dev server
  private _productUrl = './api/products/products.json';


  constructor(private _http: HttpClient) {

  }


  getProducts(): Observable<IProduct[]> {
    return this._http.get<IProduct[]>(this._productUrl)
          .do(( data) => {
            console.log('All: ', JSON.stringify(data) )
          })
          .catch(this.handleError);
  }

  private handleError( err: HttpErrorResponse) {
    console.log('Error = ', err);
    return Observable.throw(err.message);
  }

}
